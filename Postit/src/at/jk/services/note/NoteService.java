package at.jk.services.note;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import at.jk.dao.note.NoteDAO;
import at.jk.vo.note.NoteVO;



@Path("/note")
public class NoteService {
	
	int last_inserted_id = 0;
	ArrayList<String> ips = new ArrayList<>();
	/*
	@GET
	@Path("")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<PostitVO> getAllNotes() {
		return new PostitDAO().getAllNotes();
	}
	*/
	
	@GET
	@Path("")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public List<NoteVO> getAllNotes( @HeaderParam("username") String username,
									@HeaderParam("password") String password) {
		return new NoteDAO().getAllNotesByUser(password,username);
	}
	
	@GET
	@Path("/getAmountNotes")
	@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
	public String getAmountNotes( @HeaderParam("username") String username,
									@HeaderParam("password") String password) {
		System.out.println("/getAmountNotes");
		return new NoteDAO().getAmountOfNotes(password, username);
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteNote(@PathParam("id") int id,
								@HeaderParam("username") String username,
								@HeaderParam("password") String password) {

		try {
			new NoteDAO().deleteNoteById(id,password, username);
			javax.ws.rs.core.Response.ResponseBuilder rb = Response.ok();
			return rb.build();
		} catch (Exception e) {
			javax.ws.rs.core.Response.ResponseBuilder rb = Response.status(404);
			rb.entity(new String("Die angeforderten Daten konnten nicht gefunden werden!"));
			return rb.build();
		}

	}
	
	
	@POST
	@Path("")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public NoteVO addNote(NoteVO note,
			@HeaderParam("username") String username,
			@HeaderParam("password") String password){
		NoteDAO postitDAO = new NoteDAO();
		//sends the new one back to the client
		System.out.println("added note");
		return postitDAO.addNote(note, password, username);
	}
	
	@POST
	@Path("/update")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public NoteVO updateNotebyID(NoteVO note , 
									@HeaderParam("username") String username,
									@HeaderParam("password") String password){
		
		NoteDAO postitDAO = new NoteDAO();
		System.out.println(note.getPosX());
		return postitDAO.updateNote(note, password, username);
	}
	
	@POST
	@Path("/smallupdate")
	@Consumes({ MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_JSON })
	public NoteVO smallUpdateNotebyID(NoteVO note , 
									@HeaderParam("username") String username,
									@HeaderParam("password") String password){
		NoteDAO postitDAO = new NoteDAO();
		System.out.println("Updated note by id=" + note.getId());
		return postitDAO.smallUpdateNote(note, password, username);
	}
	
	
	
	@GET
	@Path("/clear")
	public void clearNotes(@HeaderParam("username") String username,
			@HeaderParam("password") String password){
		NoteDAO postitDAO = new NoteDAO();
		postitDAO.clearAllNotes(password, username);
	}
	
	@GET
	@Path("/setIP")
	@Produces({ MediaType.APPLICATION_JSON })
	public void setData(@Context HttpServletRequest request){
	   String ip = request.getRemoteAddr();
	   ips.add(ip);
	   for(String str : ips){
		   System.out.println(str);
	   }
	}

	
	


}
