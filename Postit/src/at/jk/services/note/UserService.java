package at.jk.services.note;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.mysql.jdbc.Connection;

import at.jk.Utilities.Helper;
import at.jk.dao.note.NoteDAO;
import at.jk.dao.note.UserDAO;
import at.jk.statics.StaticValues;
import at.jk.vo.note.UserVO;

@Path("/users")
public class UserService {
	
	@GET
	@Path("/exists")
	@Produces({ MediaType.APPLICATION_JSON })
	public String checkIfUserExists(@HeaderParam("password") String password,
									 @HeaderParam("username") String username){
		
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		UserVO user = new UserVO();
		user.setPassword(password);
		user.setUsername(username);
		Connection con = null;
		try {
			con = (Connection) DriverManager.getConnection(StaticValues.URL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		boolean state = Helper.checkIfUserExists(user , con);
		if (state)
			return "true";
		else
			return "false";
		
	}
	
	@GET
	@Path("")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<UserVO> getUsers(){
		NoteDAO postitDAO = new NoteDAO();
		return postitDAO.getAllUsers();
	}
	
	@GET
	@Path("/getData")
	@Produces({ MediaType.APPLICATION_JSON})
	public UserVO getUser(@HeaderParam("username") String username,
						  @HeaderParam("password") String password){
		UserDAO userDao = new UserDAO();
		return userDao.getAllUserData(password, username);
	}
	
	@POST
	@Path("/update")
	@Produces({MediaType.APPLICATION_JSON})
	public UserVO updateUser(UserVO userVO, 
							@HeaderParam("username") String username,
			  				@HeaderParam("password") String password){
		UserDAO userDAO = new UserDAO();
		return userDAO.updateUserData(userVO, password, username);
	}
	
	@POST
	@Path("/")
	@Produces({MediaType.APPLICATION_JSON})
	public String removeUser(){
		return "removed";
	}
	
}
