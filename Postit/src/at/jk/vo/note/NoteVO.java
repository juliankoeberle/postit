package at.jk.vo.note;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NoteVO {

	private int id;
	private String note_text;
	//change to float
	private float posX;
	private float posY;
	private String color;

	public NoteVO() {
		super();
	}

	
	//---------------------------------------change to float
	public NoteVO(int id, String note_text, float posX, float posY, String color) {
		super();
		this.id = id;
		this.note_text = note_text;
		this.posX = posX;
		this.posY = posY;
		this.color = color;
	}



	public String getColor() {
		return color;
	}



	public void setColor(String color) {
		this.color = color;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNote_text() {
		return note_text;
	}

	public void setNote_text(String note_text) {
		this.note_text = note_text;
	}

	public float getPosX() {
		return posX;
	}

	public void setPosX(float posX) {
		this.posX = posX;
	}

	public float getPosY() {
		return this.posY;
	}

	public void setPosY(float posY) {
		this.posY = posY;
	}
	
	
	public boolean isNoteAllowed(){
		if (this.note_text.contains("'") || this.note_text.contains(";") || this.note_text.contains("=")){
			return false;
		}
		else if (this.color.contains("'") || this.color.contains(";") || this.color.contains("=")){
			return false;
		}
		else{
			return true;
		}
	}
	
	
	

}
