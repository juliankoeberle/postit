package at.jk.vo.note;

import java.util.ArrayList;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class UserVO {
	private String username = "";
	private String firstname = "";
	private String lastname = "";
	private String password = "";
	private String email = "";
	
	private int defaultNoteWidth ;
	private int defaultNoteHeight ;
	private ArrayList<NoteVO> postits = new ArrayList<>();
	
	
	public UserVO() {
		super();
	}
	
	public UserVO(String username, String firstname, String lastname, String password, String email,
			ArrayList<NoteVO> postits) {
		super();
		this.username = username;
		this.firstname = firstname;
		this.lastname = lastname;
		this.password = password;
		this.email = email;
		this.postits = postits;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getFirstname() {
		return firstname;
	}
	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ArrayList<NoteVO> getPostits() {
		return postits;
	}
	public void setPostits(ArrayList<NoteVO> postits) {
		this.postits = postits;
	}
	
	
	public int getDefaultNoteWidth() {
		return defaultNoteWidth;
	}

	public void setDefaultNoteWidth(int defaultNoteWidth) {
		this.defaultNoteWidth = defaultNoteWidth;
	}

	public int getDefaultNoteHeight() {
		return defaultNoteHeight;
	}

	public void setDefaultNoteHeight(int defaultNoteHeight) {
		this.defaultNoteHeight = defaultNoteHeight;
	}

	//sql injection
	public boolean isUserAllowed(){
		if (this.username.contains(";") || this.username.contains(";") || this.username.contains("=")){
			return false;
		}
		else if (this.firstname.contains(";") || this.firstname.contains(";") || this.firstname.contains("=")){
			return false;
		}
		else if (this.lastname.contains(";") || this.lastname.contains(";") || this.lastname.contains("=")){
			return false;
		}
		else if (this.password.contains(";") || this.password.contains(";") || this.password.contains("=")){
			return false;
		}
		else if (this.email.contains(";") || this.email.contains(";") || this.email.contains("=")){
			return false;
		}
		else{
			return true;
		}
	}
	
	
	
}