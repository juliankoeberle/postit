package at.jk.Server;

import java.io.IOException;
import com.sun.jersey.api.container.httpserver.HttpServerFactory;
import com.sun.net.httpserver.HttpServer;

public class NoteServer {
	public static void main(String[] args) throws IllegalArgumentException, IOException {

		//HttpServer server = HttpServerFactory.create("http://10.51.51.12:8078/");
		HttpServer server = HttpServerFactory.create("http://localhost:8080/");
		server.start();
	}

}
