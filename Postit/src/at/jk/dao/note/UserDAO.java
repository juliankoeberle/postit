package at.jk.dao.note;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import at.jk.Utilities.Helper;
import at.jk.statics.StaticValues;
import at.jk.vo.note.NoteVO;
import at.jk.vo.note.UserVO;

public class UserDAO {
	
	public UserDAO() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
	
	public Connection getConnection(){
		
		try {
			return DriverManager.getConnection(StaticValues.URL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public UserVO getAllUserData(String password, String username){
		UserVO user = new UserVO();
		user.setUsername(username);
		user.setPassword(password);
		
		if (Helper.checkIfUserExists(user,getConnection())){	
				
				try {
					Connection connection = getConnection();
					Statement stmt = (Statement) connection.createStatement();      
					ResultSet resultSet = stmt.executeQuery("select * from User where username='"+username+"';");

					resultSet.first();
					
					if(resultSet.absolute(1)){
						user.setEmail(resultSet.getString("email"));
						user.setFirstname(resultSet.getString("firstname"));
						user.setLastname(resultSet.getString("lastname"));
						user.setPassword(resultSet.getString("password"));
						user.setUsername(resultSet.getString("username"));
						user.setDefaultNoteHeight(resultSet.getInt("defaultHeight"));
						user.setDefaultNoteWidth(resultSet.getInt("defaultWidth"));
					}
					else {
						//no data found
						return null;
					}
					stmt.close();
					connection.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			return user;
		}
		//sql probelem or not found
		return null;
	}
	public UserVO updateUserData(UserVO userVO, String password, String username){
		UserVO user = new UserVO();
		user.setUsername(username);
		user.setPassword(password);
		Connection c = getConnection();
		if (Helper.checkIfUserExists(user , getConnection())){
			try {
				Statement stmt = (Statement) c.createStatement();
				if (userVO.isUserAllowed()){
					System.out.println(userVO.getDefaultNoteHeight());
					System.out.println(userVO.getDefaultNoteWidth());
					String update =  "UPDATE User SET  password= '"+userVO.getPassword()+"' , firstname= '"+userVO.getFirstname()+"', lastname='"+userVO.getLastname()+"' , email='"+userVO.getEmail() + "' , defaultWidth ="+userVO.getDefaultNoteWidth()+" , defaultHeight = "+userVO.getDefaultNoteHeight()+" where username ='"+userVO.getUsername()+"' ;";
					
					//System.out.println(update);
					stmt.executeUpdate(update);
					
					user.setEmail(userVO.getEmail());
					user.setFirstname(user.getFirstname());
					user.setLastname(userVO.getLastname());
					user.setPassword(userVO.getPassword());
					user.setDefaultNoteHeight(userVO.getDefaultNoteHeight());
					user.setDefaultNoteWidth(userVO.getDefaultNoteWidth());
				}
				else {
					return null;
				}
				stmt.close();
				c.close();
			} catch (SQLException e){
				e.printStackTrace();
			} finally {
				
			}
			return user;
		}
		else{
			//no access 
			//password or username is wrong
			return null;
		}
	}
	public void deleteUser(String password, String username){
	}
}
