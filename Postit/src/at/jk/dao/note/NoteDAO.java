package at.jk.dao.note;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;


import at.jk.Utilities.Helper;

import at.jk.statics.StaticValues;
import at.jk.vo.note.NoteVO;
import at.jk.vo.note.UserVO;



public class NoteDAO {
	//Connection
	//private Connection connection = null;
	
	public NoteDAO() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

	}
	
	public Connection getConnection(){
		
		try {
			return DriverManager.getConnection(StaticValues.URL);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	//-----------
	
	
	public List<NoteVO> getAllNotesByUser(String password ,String username) {
		List<NoteVO> notes = new ArrayList<>();
		UserVO user = new UserVO();
		user.setUsername(username);
		user.setPassword(password);
		Connection c = getConnection();
		  
		if (Helper.checkIfUserExists(user, getConnection())){
			try{
				//Connection c = getConnection();
				Statement stmt = (Statement) c.createStatement();      
				ResultSet resultSet = stmt.executeQuery("select * from Note where fk_username='"+username+"';");

				resultSet.first();
				
				if(resultSet.absolute(1)){
					while (!(resultSet.isAfterLast())) {
						//posX  posY to float
						NoteVO postit = new NoteVO(resultSet.getInt("id"),
														resultSet.getString("note_text"), 
														resultSet.getFloat("posX"), 
														resultSet.getFloat("posY"),
														resultSet.getString("color"));
						
						notes.add(postit);
						resultSet.next();
					}
					c.close();
					return notes;
				} else {
					return null;
				}
				
			}catch(SQLException e){
				try {
					c.close();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				System.out.println("Somthing went wrong by getting all notes by user");
			}finally{
				
			}
		}
		else {
			return null;
		}
		return notes;
	}

	public void deleteNoteById(int id, String password, String username) {
		
		UserVO user = new UserVO();
		user.setUsername(username);
		user.setPassword(password);
		Connection c = getConnection();
		if (Helper.checkIfUserExists(user, getConnection())){
			try{
				Statement stmt = (Statement) c.createStatement();
				stmt.executeUpdate("delete from Note where id="+id+" and fk_username='"+username+"';");
				System.out.println("Note with id: "+id+" has been deleted");
				stmt.close();
				c.close();
			}catch(SQLException e){
				System.out.println("Somthing went wrong by deleting this note");
			}finally{
			
			}
		}
		
	}

	public NoteVO addNote(NoteVO note, String password, String username) {
		UserVO user = new UserVO();
		user.setUsername(username);
		user.setPassword(password);
		Connection c = getConnection();
		if (Helper.checkIfUserExists(user ,getConnection())){
			if (Integer.parseInt(this.getAmountOfNotes(password, username))<=30){
				int result  = 0; 
				NoteVO noteVO = new NoteVO();
				try {
					if (note.isNoteAllowed()){
						Statement stmt = (Statement) c.createStatement();
						String insert = "insert into Note (note_text, posX, posY, color ,fk_username) values ("+"'"+note.getNote_text()+"'"+", "+"'"+note.getPosX()+"'"+", "+"'"+note.getPosY()+"'"+", "+"'"+note.getColor()+"'"+", "+"'"+username+"'"+");";
						stmt.executeUpdate(insert,Statement.RETURN_GENERATED_KEYS);
						
						ResultSet rs = stmt.getGeneratedKeys();
						if (rs.next()){
						    result=rs.getInt(1);
						}
						noteVO.setColor(note.getColor());
						noteVO.setId(result);
						noteVO.setPosX(note.getPosX());
						noteVO.setPosY(note.getPosY());
						noteVO.setNote_text(note.getNote_text());
						System.out.println(noteVO.getNote_text());
						
						stmt.close();
					}
					
					c.close();
				} catch (SQLException e){
					e.printStackTrace();
				} 
				try {
					c.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				return noteVO;
			}
			else{
				//To much notes
				return null;
			}
		}
		else{
			try {
				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
	}
	public NoteVO updateNote(NoteVO note, String password ,String username){
		NoteVO noteVO = new NoteVO();
		UserVO user = new UserVO();
		user.setUsername(username);
		user.setPassword(password);
		Connection c = getConnection();
		if (Helper.checkIfUserExists(user , getConnection())){
			try {
				if (note.isNoteAllowed()){
					Statement stmt = (Statement) c.createStatement();
					
					String update =  "UPDATE Note SET  posX="+note.getPosX()+" ,posY="+note.getPosY()+ ",note_text='"+note.getNote_text()+"', color='"+note.getColor()+"' where id="+note.getId()+";";
					stmt.executeUpdate(update);
					
					noteVO.setColor(note.getColor());
					noteVO.setId(note.getId());
					noteVO.setPosY(note.getPosY());
					noteVO.setPosX(note.getPosX());
					noteVO.setNote_text(note.getNote_text());
					System.out.println("response note" + noteVO.getNote_text());
					
					stmt.close();
				}
				c.close();
				
			} catch (SQLException e){
				e.printStackTrace();
			} finally {
				/*try {
					c.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
			return noteVO;
		}
		else{
			//no access 
			//password or username is wrong
			return null;
		}
	
		
	}
	
	public String getAmountOfNotes(String password , String username){
		UserVO userVO = new UserVO();
		userVO.setPassword(password);
		userVO.setUsername(username);
		Connection c = getConnection();
		if (Helper.checkIfUserExists(userVO , getConnection())){
			try {
				Statement stmt = (Statement) c.createStatement();
				System.out.println(userVO.getUsername());
				String number = "SELECT COUNT(*) as `total`from Note WHERE Note.fk_username = '" + userVO.getUsername() + "';"; 
				ResultSet resultSet =  stmt.executeQuery(number);
				
				resultSet.first();
				return resultSet.getString("total");
			}
			catch (SQLException e){
				e.printStackTrace();
			}
		}
		try {
			c.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	//no posX update
	public NoteVO smallUpdateNote(NoteVO note, String password ,String username){
		NoteVO noteVO = new NoteVO();
		UserVO user = new UserVO();
		user.setUsername(username);
		user.setPassword(password);
		Connection c = getConnection();
		if (Helper.checkIfUserExists(user , getConnection())){
			try {
				Statement stmt = (Statement) c.createStatement();
				if (note.isNoteAllowed()){
					String update =  "UPDATE Note SET  posX="+note.getPosX()+" , posY="+note.getPosY()+ ",note_text='"+note.getNote_text()+"', color='"+note.getColor()+"' where id="+note.getId()+";";
					stmt.executeUpdate(update);
					
					noteVO.setColor(note.getColor());
					noteVO.setId(note.getId());
					noteVO.setPosY(note.getPosY());
					noteVO.setPosX(note.getPosX());
					noteVO.setNote_text(note.getNote_text());
					System.out.println("response note" + noteVO.getNote_text());
					
					stmt.close();
				}
				c.close();
			} catch (SQLException e){
				e.printStackTrace();
			} 
			return noteVO;
		}
		else{
			return null;
		}
	}
	
	//TODO sql injection check
	public void clearAllNotes(String password, String username){
		UserVO user = new UserVO();
		user.setUsername(username);
		user.setPassword(password);
		Connection c = getConnection();
		if (Helper.checkIfUserExists(user, getConnection())){
			try {
				Statement stmt = (Statement) c.createStatement();
				String delete =  "delete from Note where id between 0 and 1000000000000 and fk_username='"+username+"';";
				//stmt.executeQuery(delete);
				stmt.executeUpdate(delete);
				stmt.close();
				c.close();
				System.out.println("deleted all notes");
			} catch (SQLException e){
				e.printStackTrace();
			} finally{
				/*try {
					c.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
			}
		}
	}
	public List<UserVO> getAllUsers() {
		Connection c = getConnection();
		try {
			Statement stmt = (Statement) c.createStatement();
			
			String select = "Select * from User";
			ResultSet resultSet =  stmt.executeQuery(select);
			ArrayList<UserVO> users = new ArrayList<>();
			
			resultSet.first();
			
			if(resultSet.absolute(1)){
				while (!(resultSet.isAfterLast())) {
					//posX  posY to float
					UserVO user = new UserVO(resultSet.getString("username"),
											resultSet.getString("firstname"), 
											resultSet.getString("lastname"), 
											resultSet.getString("password"),
											resultSet.getString("email"),null);
					users.add(user);
					resultSet.next();
				}

				stmt.close();
			} else {
			    System.out.println("No data");
			}
		
			stmt.close();
			c.close();
			return users;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		} finally{
			/*try {
				c.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}
		
	}
	
	/*public boolean checkIfUserExists(UserVO user){
		
		if (user.isUserAllowed()){
			try {
				Connection c = getConnection();
				Statement stmt = (Statement) c.createStatement();
				
				String select = "select * from User where username ='" + user.getUsername() + "' and password = '" + user.getPassword() +"';";
				ResultSet resultSet = stmt.executeQuery(select);
				resultSet.first();
				if(resultSet.absolute(1)){
					resultSet.next();
					c.close();
					stmt.close();
					return true;
				}
				else {
					resultSet.next();
					c.close();
					stmt.close();
					return false;
				}
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return false;
			}
		}
		else{
			//contains not allowed characters
			return false;
		}
		
	}*/

	
	

	
}
