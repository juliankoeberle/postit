$(function() {
	//for the first load
	var widthNote = 0;
	var heightNote = 0;
	getNoteSize();
	getAllNotesFromServer();
	updatePage();
	//setIP();
	
	/*
	var Alert = undefined;

	(function(Alert) {
	  var alert, error, info, success, warning, _container;
	  info = function(message, title, options) {
	    return alert("info", message, title, "icon-info-sign", options);
	  };
	  warning = function(message, title, options) {
	    return alert("warning", message, title, "icon-warning-sign", options);
	  };
	  error = function(message, title, options) {
	    return alert("error", message, title, "icon-minus-sign", options);
	  };
	  success = function(message, title, options) {
	    return alert("success", message, title, "icon-ok-sign", options);
	  };
	  alert = function(type, message, title, icon, options) {
	    var alertElem, messageElem, titleElem, iconElem, innerElem, _container;
	    if (typeof options === "undefined") {
	      options = {};
	    }
	    options = $.extend({}, Alert.defaults, options);
	    if (!_container) {
	      _container = $("#alerts");
	      if (_container.length === 0) {
	        _container = $("<ul>").attr("id", "alerts").appendTo($("body"));
	      }
	    }
	    if (options.width) {
	      _container.css({
	        width: options.width
	      });
	    }
	      alertElem = $("<li>").addClass("alert").addClass("alert-" + type);
	      setTimeout(function() {
	         alertElem.addClass('open');
	      }, 1);
	    if (icon) {
	      iconElem = $("<i>").addClass(icon);
	      alertElem.append(iconElem);
	    }
	    innerElem = $("<div>").addClass("alert-block");
	    alertElem.append(innerElem);
	    if (title) {
	      titleElem = $("<div>").addClass("alert-title").append(title);
	      innerElem.append(titleElem);
	    }
	    if (message) {
	      messageElem = $("<div>").addClass("alert-message").append(message);
	      innerElem.append(messageElem);
	    }
	    if (options.displayDuration > 0) {
	      setTimeout((function() {
	        leave();
	      }), options.displayDuration);
	    } else {
	      innerElem.append("<em>Click to Dismiss</em>");
	    }
	    alertElem.on("click", function() {
	      leave();
	    });
	     function leave() {
	         alertElem.removeClass('open');
	          alertElem.one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend',  function() { return alertElem.remove(); });
	    }
	    return _container.prepend(alertElem);
	  };
	  Alert.defaults = {
	    width: "",
	    icon: "",
	    displayDuration: 3000,
	    pos: ""
	  };
	  Alert.info = info;
	  Alert.warning = warning;
	  Alert.error = error;
	  Alert.success = success;
	  return _container = void 0;
	    
	   
	})(Alert || (Alert = {}));

	this.Alert = Alert;
	
*/
	
	//----------------------------------------------
	var password;
	var username;

	var screen = $(window);
	//var id = 0;
	//var notes = [];
	
	//colorBoxState
	var state = true;
	var ids = [];
	var biggestID = 0;
	var top = Math.random()*500;
	var left = Math.random()*1000;
	var strHTML = "";
	
	//for easyer remove
	Array.prototype.remove = function() {
	    var what, a = arguments, L = a.length, ax;
	    while (L && this.length) {
	        what = a[--L];
	        while ((ax = this.indexOf(what)) !== -1) {
	            this.splice(ax, 1);
	        }
	    }
	    return this;
	};
	
	function alert(message){
		$(".alertElement").html("<div class='alertBox'><div class='sign'> <span class='glyphicon glyphicon-info-sign'></span></div><div class='content'>"+message+"</div></div>");
		$(".alertElement").on( 'click', '.alertBox', function () {
			$(".alertElement").html("");
		});
	}


	
	$(".add-button").click(function(){
		addNote();
	})
		/*
		$('body').on('click', '.text', function(event) {
			
			if ($(this).val("Tap into note to edit text")){
				$(this).val('');
			}			
		})
		*/
	
	//move------
		$(document).on('mouseover', '.glyphicon-hand-up', function(){
			var id = $(this).parent().attr("id");			
			state = true;
			$(this).parent().draggable(
					{
				       
				        cancel : "ta",
				        scroll: true,
				        cursor : "pointer",
				        drag : function(e,ui){
				        	
				        	//to deactivate the color box if the color box is aktivated
				        	$(".color").hide();
				            if(ui.position.left < 15)    
				                ui.position.left = 15;
				            if (ui.position.top < 60)
				            	ui.position.top = 60;  
				            if (ui.position.left > (screen.width()-215))
				            	ui.position.left = screen.width()-215;
				           
				        },
				        stop : function(e, ui){
				        	//TODO: UPdate this Note about color;
				        	id = $(this).attr("id");
				      
				        	var color = "";
				        	color = $(this).css("background-color");
				        	if (color == "rgb(240, 233, 160)")
				        		color = "yellow";
				        	if (color == "rgb(255, 153, 153)")
				        		color = "pink";
				        	if (color == "rgb(51, 153, 255)")
				        		color = "blue";
				        	if (color == "rgb(153, 255, 153)")
				        		color = "green";
				        	if (color == "rgb(255, 255, 255)")
				        		color = "white";
				        	if (color == "rgb(148, 77, 255)")
				        		color = "violet";
				        	updateNote(
				        			id,
				        			$("#"+id).find(".text").val(),
				        			$("#"+id).offset().top ,
				        			$("#"+id).offset().left ,
				        			color
				        	);
				        }
				    })
		})
		$(document).on("mouseleave", ".glyphicon-hand-up", function() {
			try{
				$(this).parent().draggable("destroy");
			}
			catch (e){
				
			}
		});
		
		//delte--------
		$('body').on('click', '.glyphicon-trash', function(event) {
			if (confirm('Are you sure you want do delete this fucking note?')){
		    	$(".color").hide();
		        state  = true;
		        var index = ids.indexOf($(this).attr("id"));
		        if (index > -1) {
		            ids.splice(index, 1);
		          	console.log(ids);
		        }
		        $(this).parent().remove();
		        //true = with Alert 
		        //false = without Alert
		        removeNote(parseInt($(this).parent().attr("id")),false);
		        //TODO remove this note from server;
		    }
		})
		
		
		
		//color------------------------
		var idOfNotes = "";
		$('body').on('click','.glyphicon-option-vertical',function(){
			if (state){
				var positionNote = $(this).parent().offset();
				idOfNote = $(this).parent().prop('id');
				if (positionNote.top < 100){
					positionNote.top = 320;
				}
				$(".color").css({
					top : positionNote.top - 100 +'px',
					left : positionNote.left - 50 + 'px',
				})
				$(".color").show();
				state = false;
				
			}
			else{
				$(".color").hide();
				state = true;
			}
			
		})
		$(".pink").hover(function(){
			$("#"+idOfNote).css({background: "#ff9999"});
		})
		$(".blue").hover(function(){
			$("#"+idOfNote).css({background: "#3399ff"});
		})
		$(".green").hover(function(){
			$("#"+idOfNote).css({background: "#99ff99"});
		})
		$(".yellow").hover(function(){
			$("#"+idOfNote).css({background: "#f0e9a0"});
		})
		$(".white").hover(function(){
			$("#"+idOfNote).css({background: "white"});
		})
		$(".violet").hover(function(){
			$("#"+idOfNote).css({background: "#944dff"});
		})
	
		
		$(".pink").click(function(){
			$(this).parent().hide();
			updateNote(idOfNote, 
					$("#"+idOfNote).find(".text").val(),
					$("#"+idOfNote).offset().top,
					$("#"+idOfNote).offset().left,
					"pink");
			state = true;
		})
		$(".blue").click(function(){
			$(this).parent().hide();
			updateNote(idOfNote, 
					$("#"+idOfNote).find(".text").val(),
					$("#"+idOfNote).offset().top,
					$("#"+idOfNote).offset().left,
					"blue");
			state = true;
		})
		$(".green").click(function(){
			$(this).parent().hide();
			updateNote(idOfNote, 
					$("#"+idOfNote).find(".text").val(),
					$("#"+idOfNote).offset().top,
					$("#"+idOfNote).offset().left,
					"green");
			state = true;
		})
		$(".yellow").click(function(){
			$(this).parent().hide();
			updateNote(idOfNote, 
					$("#"+idOfNote).find(".text").val(),
					$("#"+idOfNote).offset().top,
					$("#"+idOfNote).offset().left,
					"yellow");
			state = true;
		})
		$(".white").click(function(){
			$(this).parent().hide();
			updateNote(idOfNote, 
					$("#"+idOfNote).find(".text").val(),
					$("#"+idOfNote).offset().top,
					$("#"+idOfNote).offset().left,
					"white");
			state = true;
		})
		$(".violet").click(function(){
			$(this).parent().hide();
			updateNote(idOfNote, 
					$("#"+idOfNote).find(".text").val(),
					$("#"+idOfNote).offset().top,
					$("#"+idOfNote).offset().left,
					"violet");
			state = true;
		})
		
		//text change funktion
	
		$('body').on('change', '.text', function() {
			console.log("text changed");
			var idOfChangedNote = $(this).parent().prop('id');
			var color = "";
        	color = $("#"+idOfChangedNote).css("background-color");
        	console.log(color);
        	if (color == "rgb(240, 233, 160)")
        		color = "yellow";
        	if (color == "rgb(255, 153, 153)")
        		color = "pink";
        	if (color == "rgb(51, 153, 255)")
        		color = "blue";
        	if (color == "rgb(153, 255, 153)")
        		color = "green";
        	if (color == "rgb(255, 255, 255)")
        		color = "white";
        	if (color == "rgb(148, 77, 255)")
        		color = "violet";
        	var str = $("#"+idOfChangedNote).find(".text").val();
        	if((str.indexOf("'") > -1) || (str.indexOf("=") > -1) || (str.indexOf(";") > -1)){
        		//alert("this characters ' = ;  are not allowed");
        		Alert.error("this characters ' = ;  are not allowed");
        	}
        	else {
        		updateNote(idOfChangedNote, 
    					$("#"+idOfChangedNote).find(".text").val(),
    					$("#"+idOfChangedNote).offset().top,
    					$("#"+idOfChangedNote).offset().left,
    					color); 
        	}
			
		});
		
		
		

		
		
		
		//clear-------
		
		$(".clearbutton").click(function(){
			if (confirm('Are you sure you want do delete all notes?')) {
				console.log();
				$.ajax({
					  type: "GET",
					  url: "/Postit/api/note/clear",
					  beforeSend: function (request){
						  	console.log(password);
						  	console.log(username)
			                request.setRequestHeader("password", password);
			                request.setRequestHeader("username", username);
			                request.setRequestHeader("Content-Type", "application/json");
			                
			          },
			          success : function(data){
			        	  Alert.success('Cleared all Notes');
			          },
			          error : function(){
			        	 Alert.error("smothing went wrong"); 
			          },
			        
					  contentType: "application/json; charset=utf-8"
				});
				$(".note-contain").html("");
			}
			ids = [];
		})
		
		
		//-------------------------------------------
		
		//tidy function
		
		$(".tidybutton").click(function(){
			getNoteSize();
			tidy(true);
		})
		var resizeState = true;
		$(window).resize(function(){
			
			if ((screen.width()<= 620) && resizeState){
				$(".note").css({left : "10%"});
				resizeState = false;
			}
			else if ((screen.width()>620) && !resizeState){
				strHTML = "";
				getAllNotesFromServer();
				resizeState = true;
			}
		})
		
		function tidy(state){
			var height = parseInt(heightNote)+10;
			var widthEl = parseInt(widthNote)+10;
			console.log("swag" + height);
			console.log("swag" + widthEl);
			var row = 0.2;
			var left = 0; // 0
			var top = (row*height);
			var width = $( window ).width();
			var ammountOfNotesInARow = parseInt(width/widthEl);
			var rest = (width-(ammountOfNotesInARow * widthEl))*0.5;
			var counter = 1;
			left = rest;
			for (var i = 0; i<= biggestID+1; i++){
				if (ids.indexOf(i) != -1){
					console.log(biggestID);
					if(counter <= ammountOfNotesInARow){
						top = row * height;
						console.log(counter);
						/*
						$("#"+i).css({
							top : top+30 +"px",
							left : left +"px"
						})*/
						$("#"+i).animate({
							top : top+30  +"px",
							//left : left  / screen.width() *100+"%"
							left : left + "px"
						});
						
						var color = "";
			        	color = $("#" + i).css("background-color");
				        if (color == "rgb(240, 233, 160)")
				        	color = "yellow";
				        if (color == "rgb(255, 153, 153)")
				        	color = "pink";
				        if (color == "rgb(51, 153, 255)")
				        	color = "blue";
				        if (color == "rgb(153, 255, 153)")
				        	color = "green";
				        if (color == "rgb(255, 255, 255)")
				        	color = "white";
				        if (color == "rgb(148, 77, 255)")
							   color = "violet";
				       
				     	if (state){
							updateNote(
									i,
									$("#"+i).find(".text").val(),
									top+30,
									left,
									color);
							
				     	}
						left = left + widthEl;
						counter++;
						
						
					}
					else{
						row++;
						counter = 2;
						left = rest;
						top = row * height;
						$("#"+i).animate({
							top : top+30  +"px",
							//left : left  / screen.width() *100+"%"
							left : left + "px"
						});
						var color = "";
			        	color = $("#" + i).css("background-color");
				        if (color == "rgb(240, 233, 160)")
				        	color = "yellow";
				        if (color == "rgb(255, 153, 153)")
				        	color = "pink";
				        if (color == "rgb(51, 153, 255)")
				        	color = "blue";
				        if (color == "rgb(153, 255, 153)")
				        	color = "green";
				        if (color == "rgb(255, 255, 255)")
				        	color = "white";
				        if (color == "rgb(148, 77, 255)")
							   color = "violet";
				       
				     
						updateNote(
								i,
								$("#"+i).find(".text").val(),
								top+30,
								left,
								color);
						
						left = left + widthEl;
						console.log(left);
						
					}
				} 

			}
		}
		
		
		//--------------------ajax stuff
		
		function getAllNotesFromServer(){
			getNoteSize();
			var scroll = $(window).scrollTop();
			var result;
			var string = document.cookie;
			result = string.split(",");
		
			
			password = result[2];
			username = result[1];
			//chaned url
			
			$.ajax({
				  type: "GET",
				  url: "/Postit/api/note",
				  beforeSend: function (request){
		                request.setRequestHeader("password", password);
		                request.setRequestHeader("username", username);
		                request.setRequestHeader("Content-Type", "application/json");
		                
		          },
				  error : function(){
					  //$(".alert").show();
					  Alert.error("No internet connection");
				  },
				  contentType: "application/json; charset=utf-8"
			}).done(function(data){
			
				  if (strHTML != JSON.stringify(data)){
						strHTML = JSON.stringify(data);
						ids = [];
						$(".note-contain").html("");
						if (data == null){
							console.log("noteVO list  is null");
						}
						else if(data.noteVO && Array === data.noteVO.constructor){
							for (var i  = 0 ; i < data.noteVO.length; i++){
								console.log("noteText" + data.noteVO[i].note_text);
								var color = ""
								if (data.noteVO[i].color == "pink"){
									color = "#ff9999";
								}
								if (data.noteVO[i].color == "blue"){
									color = "#3399ff";
								}
								if (data.noteVO[i].color == "green"){
									color = "#99ff99";
								}
								if (data.noteVO[i].color == "yellow"){
									color = "#f0e9a0";
								}
								if (data.noteVO[i].color == "white"){
									color = "white";
								}
								if (data.noteVO[i].color =="violet"){
									color = "#944dff";
								}
								if (screen.width()<= 620){
									data.noteVO[i].posX = 10;
								}
							 	
							       	$("<div  id='"+ parseInt(data.noteVO[i].id) +"' class='note pinup'><div class='glyphicon glyphicon-option-vertical'></div><div class='glyphicon glyphicon-hand-up'></div><div class='glyphicon glyphicon-trash'></div><textarea class='text' cols='30' rows='7'>"+data.noteVO[i].note_text+"</textarea> </div>").appendTo(".note-contain").css({
									position : "absolute",
									top: data.noteVO[i].posY  + "px",
									left: data.noteVO[i].posX + "px",
									background : color
								})
								//very important
								//get a array of the ids of the generated notes;
								ids.push(parseInt(data.noteVO[i].id));
								//save the last array 
								//important for the tidy function
								biggestID = parseInt(data.noteVO[i].id);
							}
						}
						else{
							console.log("noteText" + data.noteVO.note_text);
							var color = ""
							if (data.noteVO.color == "pink"){
								color = "#ff9999";
							}
							if (data.noteVO.color == "blue"){
								color = "#3399ff";
							}
							if (data.noteVO.color == "green"){
								color = "#99ff99";
							}
							if (data.noteVO.color == "yellow"){
								color = "#f0e9a0";
							}
							if (data.noteVO.color == "white"){
								color = "white";
							}
							if (data.noteVO.color =="violet"){
								color = "	#944dff";
							}
							if (screen.width()<= 620){
								data.noteVO.posX = 10;
							}
						 	
										
							$("<div  id='"+ parseInt(data.noteVO.id) +"' class='note pinup'><div class='glyphicon glyphicon-option-vertical'></div><div class='glyphicon glyphicon-hand-up'></div><div class='glyphicon glyphicon-trash'></div><textarea class='text'  cols='30' rows='7' >"+data.noteVO.note_text+"</textarea> </div>").appendTo(".note-contain").css({
								position : "absolute",
								top: data.noteVO.posY  + "px",
								left: data.noteVO.posX + "px",
								background : color
							})
							//very important
							//get a array of the ids of the generated notes;
							ids.push(parseInt(data.noteVO.id));
							//save the last array 
							//important for the tidy function
							biggestID = parseInt(data.noteVO.id);
							
							
							
						}
						console.log("updated screen");
							
					}
				  $(window).scrollTop(scroll);
				  //$(".alert").hide();
				  getNoteSize();
			});
		
		}

		//add a Note to the list and send this new List to the Server
		var a = 0;
		function addNote(){
			getNoteSize();
			if (ids.length<=30){
				top = Math.random()*500;
				left = Math.random()*1200;
				
				if (top > screen.height()-300)
					top = screen.height()-300;
				if (top < 60)
					top = 60;
				if (left > screen.width()-300)
					left = screen.width()-300;
				if (left < 10)
					left = 10;
				
	//Done Math round to float		
				var note = {
						posX : left,
						posY : top,
						note_text : "Hello!",
						color : "yellow"		
				};
				console.log("screenwidth" + screen.width());
				console.log(left + "," + top);
				console.log(note.posX + "," + note.posY);
				var idOfGeneratedNote = 0;
				$.ajax({
					  type: "POST",
					  url: "/Postit/api/note/",
					  data: JSON.stringify(note),
					  success: function(data){
						  
						 	ids.push(parseInt(data.id));
						 	biggestID = parseInt(data.id);
						 	
							$("<div  id='"+data.id+"' class='note pinup'><div class='glyphicon glyphicon-option-vertical'></div><div class='glyphicon glyphicon-hand-up'></div><div class='glyphicon glyphicon-trash'></div><textarea class='text'  cols='30' rows='7' >"+data.note_text+"</textarea> </div>").appendTo(".note-contain").css({
								position : "absolute",
								top: data.posY + "px",
								left: data.posX + "px"
							})
							getNoteSize();
					  },
					  beforeSend: function (request){
			                request.setRequestHeader("password", password);
			                request.setRequestHeader("username", username);
			                request.setRequestHeader("Content-Type", "application/json");
			                
			          },
					  
					  contentType: "application/json; charset=utf-8"
				});
				console.log(note);
			}
			else{
				alert("too much notes, only 30 are allowed");
			}
			
			getNoteSize();
			
			
		}
		
		//update the Note on the server
		function updateNote(idValue,textValue,topValue,leftValue,colorValue){
			if (screen.width() > 620){
				var note = {
					id : idValue,
					posX : leftValue ,
					posY : topValue,
					note_text : textValue,
					color : colorValue
					
				}
				console.log(note);
				$.ajax({
					  type: "POST",
					  url: "/Postit/api/note/update",
					  data: JSON.stringify(note),
					  success: function(){
							console.log("successfully updated");
					  },
					  beforeSend: function (request){
			                request.setRequestHeader("password", password);
			                request.setRequestHeader("username", username);
			                request.setRequestHeader("Content-Type", "application/json");
			                
			          },
					  contentType: "application/json; charset=utf-8"
				});
			}
			else{
				var note = {
						id : idValue,
						posY : topValue ,
						note_text : textValue,
						color : colorValue
						
					}
					console.log(note.posX);
					$.ajax({
						  type: "POST",
						  url: "/Postit/api/note/smallupdate",
						  data: JSON.stringify(note),
						  success: function(){
								console.log("successfully updated");
						  },
						  beforeSend: function (request){
				                request.setRequestHeader("password", password);
				                request.setRequestHeader("username", username);
				                request.setRequestHeader("Content-Type", "application/json");
				                
				          },
						  contentType: "application/json; charset=utf-8"
					});
			}
			
		}
		//remove 
		function removeNote(id,state){
			ids.remove(id);
				$.ajax({
					  type: "DELETE",
					  url: "/Postit/api/note/"+id,
					  success: function(data){
						  //if(state){
						  	//alert("successfully deleted");
							  
							Alert.success("successfully deleted");
						  //}
					  },
					  beforeSend: function (request){
			                request.setRequestHeader("password", password);
			                request.setRequestHeader("username", username);
			                request.setRequestHeader("Content-Type", "application/json");
			                
			          },
					  contentType: "application/json; charset=utf-8"
				});
		}
		
		function updatePage(){
			window.setInterval(function(){
				//if (isAllowed){
					getAllNotesFromServer();
				//}
			}, 1500);
			$(".username").html(username);
		}
		function setIP(){
			$.ajax({
				  type: "DELETE",
				  url: "/Postit/api/note/setIP",
			});
		}
		
		$(".logout").click(function(){
			document.cookie = "user=;";
			window.location.href = '/Postit/login.html';
		})
		
		function setNoteStyle(){
			//set css
			$(".note").css("width",widthNote);
			$(".note").css("height",heightNote);
			$("textarea").css("width", widthNote-20);
			$("textarea").css("height", heightNote-30);
		}
		function getNoteSize(){
			var string = document.cookie;
			var result = string.split(",");
			widthNote = result[3];
			heightNote = result[4];
			console.log("width" +widthNote);
			console.log("height" +heightNote);
			setNoteStyle();
		}
		
		
		
})


