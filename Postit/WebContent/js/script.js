$(document).ready(function(){



	$(".glyphicon-menu-hamburger").hide();
	$(".glyphicon-remove").hide();

	var p = $(".programm");
	var position = p.position();



	var scrollValue = 50;

	$(".scrollButton").click(function() {
		var body = $("html, body");
		body.stop().animate({
			scrollTop: position.top - scrollValue
		}, '500', 'swing', function() { 
   			
		});
	})

	var lastScrollTop = 0;
	/*
	$(window).scroll(function(){
	
		var st = $(this).scrollTop();
   		if (st > lastScrollTop){
       		$("#menu").css("height","50px");
			$("#menu").css("line-height", "50px");
			$("#menu .page").css("height", "50px");
			$(".glyphicon-menu-hamburger").css("margin-top","20px");
			$(".glyphicon-remove").css("margin-top","20px");
			$("#smallMenu").css("top","50px");
   		} else {
      		$("#menu").css("height","100px");
			$("#menu").css("line-height", "100px");
			$("#menu .page").css("height", "100px");
			$(".glyphicon-menu-hamburger").css("margin-top","40px");
			$(".glyphicon-remove").css("margin-top","40px");
			$("#smallMenu").css("top","100px");
   		}
   		lastScrollTop = st;
	})
	*/

	if ($(window).width() < 1000) {

	   		$(".page").hide();
	   		$(".glyphicon-menu-hamburger").show();
		}
		else {
	   		$(".page").show();
	   		$(".glyphicon-menu-hamburger").hide();
	   		$(".glyphicon-remove").hide();
	   		//$( "#smallMenu" ).hide();
	   		
	}


	$(window).resize(function(){
		if ($(window).width() < 1000) {
	   		$(".page").hide();
	   		$(".glyphicon-menu-hamburger").show();
		}
		else {
	   		$(".page").show();
	   		$(".glyphicon-menu-hamburger").hide();
	   		$(".glyphicon-remove").hide();
	   		//$( "#smallMenu" ).hide();
		}
	})


	var pages = $(".pages").html();
	var state = true;
	$(".glyphicon-menu-hamburger").click(function(){
		$(".glyphicon-menu-hamburger").hide();
		$(".glyphicon-remove").show();

		if (state){
			state = false;
			$( "#smallMenu" ).show( "slow", function() {
				$("#smallMenu").html(pages);
				$("#smallMenu .page").css("margin-top","40px");
				$("#smallMenu .page").css("font-size","200%");
				$("#smallMenu .page").css("width","100%");
				$("#smallMenu .page").show();
	    		
	  		});
		}
	})


	$(".glyphicon-remove").click(function(){
		$(".glyphicon-remove").hide();
	    $(".glyphicon-menu-hamburger").show();
		if (!state){
			state = true;
			$( "#smallMenu" ).hide( "slow", function() {
	  		})
		}
	})


	$(".thumbnail").hover(function(){
		var id = $(this).attr('id');
		var width = $("#" + id).width() + 1;
		var height = $("#"+ id).height() + 1 ;
		console.log(width);
		var hoverElement = $("#" + id +  " .infoperson")
		hoverElement.css("width",width+"px");
		hoverElement.css("height",height+"px");
		hoverElement.show();
	}, function(){
		var id = $(this).attr('id');
		var hoverElement = $("#" + id +  " .infoperson")
		hoverElement.hide();
	})


})


