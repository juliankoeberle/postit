$(function() {
	loadUserData();
	loadColor();
	var settings = $(".settings").hide();
	$('#settingsUsername').prop('disabled', true);
	var password;
	var email;
	var firstname;
	var lastname;
	var password;
	var username;
	
	var oldPassword; 
	
	var width;
	var height;
	
	function alert(message){
		$(".alertElement").html("<div class='alertBox'><div class='sign'> <span class='glyphicon glyphicon-info-sign'></span></div><div class='content'>"+message+"</div></div>");
		$(".alertElement").on( 'click', '.alertBox', function () {
			$(".alertElement").html("");
		});
	}
	
	
	function loadUserData(){
		//TODO load from width and hieght from user;
		var string = document.cookie;
		var result = string.split(",");
		password = result[2];
		username = result[1];
		$("#settingsUsername").val(username);
		oldPassword = password;
		
		
		
		$.ajax({
			  type: "GET",
			  url: "/Postit/api/users/getData",
			  beforeSend: function (request){
	                request.setRequestHeader("password", password);
	                request.setRequestHeader("username", username);
	                request.setRequestHeader("Content-Type", "application/json");
	                
	          },
			  error : function(){
				  $(".alert").show();
			  },
			  contentType: "application/json; charset=utf-8"
		}).done(function(data){
			email = data.email;
			firstname = data.firstname;
			lastname = data.lastname;
			password = data.password;
			width = data.defaultNoteWidth;
			height = data.defaultNoteHeight;
			setNoteStyle();
		});
		
	}

	$("#userSettings").click(function(){
		loadUserData();
		$("#settingsPassword").val(password);
		$("#settingsEmail").val(email);
		$("#settingsFirstname").val(firstname);
		$("#settingsLastname").val(lastname);	
		$("#settingsWidth").val(width);
		$("#settingsHeight").val(height);
		/*$(".note-contain").css("-webkit-filter","brighness(10%)");*/
		
		settings.show();
		
		
	});
	
	function setNoteStyle(){
		//set css
		$(".note").css("width",width);
		$(".note").css("height",height);
		$("textarea").css("width", width-20);
		$("textarea").css("height", height-30);
	}
	
	$("#saveChanges").click(function(){
		
		password = $("#settingsPassword").val();
		email = $("#settingsEmail").val();
		firstname = $("#settingsFirstname").val();
		lastname = $("#settingsLastname").val();
		width = $("#settingsWidth").val();
		height = $("#settingsHeight").val();
		
		//set css
		setNoteStyle();
	
		
		user = {
				password : password,
				username : username,
				firstname: firstname,
				lastname : lastname,
				email : email,
				defaultNoteWidth :  width,
				defaultNoteHeight : height
		}
		
		$.ajax({
			  type: "POST",
			  url: "/Postit/api/users/update",
			  data: JSON.stringify(user),
			  success: function(data){
				  //update cookie
				  document.cookie = "user="+","+username + "," + password+"," + width + "," + height + ",";
				  alert("done");
			  },
			  beforeSend: function (request){
	                request.setRequestHeader("password", oldPassword);
	                request.setRequestHeader("username", username);
	                request.setRequestHeader("Content-Type", "application/json");
	                
	          },
			  
			  contentType: "application/json; charset=utf-8"
		});
	});
	$("#close").click(function(){
		settings.hide();
		/*$(".note-contain").css("-webkit-filter","brighness(100%)");*/
	});
	
	$(".greenGUI").hover(function(){
		setGreen();
	});
	$(".redGUI").hover(function(){
		setRed();
	});
	$(".darkGUI").hover(function(){
		setDark();
	});
	$(".violetGUI").hover(function(){
		setViolet();
	});
	
	function setGreen(){
		$(".menu").css("background","#339966");
		$(".tidybutton").css("background", "#194d33");
		$(".clearbutton").css("background", "#194d33");
		$(".settings").css("background" , "#339966");
		$("#saveChanges").css("background" , "#194d33");
		$(".add-button").css("background", "#339966");
		$(".title").css("color", "#194d33");
		$(".dropdown-toggle").css("background" , "#194d33");
		document.cookie = "color=green";
	}
	function setRed(){
		$(".menu").css("background","#B40404");
		$(".tidybutton").css("background", "#610B0B");
		$(".clearbutton").css("background", "#610B0B");
		$(".settings").css("background" , "#B40404");
		$("#saveChanges").css("background" , "#610B0B");
		$(".add-button").css("background", "#B40404");
		$(".title").css("color", "#610B0B");
		$(".dropdown-toggle").css("background" , "#610B0B");
		document.cookie = "color=red";
	}
	function setDark(){
		$(".menu").css("background","#1C1C1C");
		$(".tidybutton").css("background", "#6E6E6E");
		$(".clearbutton").css("background", "#6E6E6E");
		$(".settings").css("background" , "#1C1C1C");
		$("#saveChanges").css("background" , "#6E6E6E");
		$(".add-button").css("background", "#1C1C1C");
		$(".title").css("color", "#6E6E6E");
		$(".dropdown-toggle").css("background" , "#6E6E6E");
		document.cookie = "color=dark";
	}
	function setViolet(){
		$(".menu").css("background","#8000ff");
		$(".tidybutton").css("background", "#4B088A");
		$(".clearbutton").css("background", "#4B088A");
		$(".settings").css("background" , "#8000ff");
		$("#saveChanges").css("background" , "#4B088A");
		$(".add-button").css("background", "#8000ff");
		$(".title").css("color", "#4B088A");
		$(".dropdown-toggle").css("background" , "#4B088A");
		document.cookie = "color=violet";
	}
	
	function loadColor(){
		var color = getCookie("color");
		if (color == 'green'){
			setGreen();
		}
		else if (color == 'red'){
			setRed();
		}
		else if (color == 'dark'){
			setDark();
		}
		else if (color == 'violet'){
			setViolet();
		}
	}
	function getCookie(name) {
		var value = "; " + document.cookie;
		var parts = value.split("; " + name + "=");
		if (parts.length == 2) return parts.pop().split(";").shift();
	}
	
	
});

